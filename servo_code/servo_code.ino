
#include <Servo.h>

Servo water_dispenser_servo;
Servo ramp_servo;
int on = 0;
int off = 0;
double time_on = 0;
int time_rx = 0;
int device = -1;

void setup() {
  Serial.begin(9600);
  water_dispenser_servo.attach(9);
  ramp_servo.attach(10);
}

void loop() {
  while(Serial.available() >= 4){
    device = Serial.read();
    on = Serial.read();
    off = Serial.read();
    time_rx = Serial.read();    
    if(device == 0){                  // water dispenser
        time_on = time_rx  / 2.0;
        if(on <= 180 && on >= 0 && off <= 180 && off >= 0){
          if(on == off){
            water_dispenser_servo.write(on);
          }
          else{
            water_dispenser_servo.write(on);
            delay(time_on * 1000);
            water_dispenser_servo.write(off);
          }
          Serial.println(on);
          Serial.println(off);
          Serial.println(time_rx);
        }        
    }
    else{                               // ramp
        if(on <= 180 && on >= 0){
          ramp_servo.write(on);
          Serial.println(on);
        }
    }  
  }
}
