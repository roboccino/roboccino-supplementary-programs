#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"

int chosen_motor = 0;
int rpm = 0;
int steps = 0;
int max_steps = 200;
int dc_speed = 0;
int dc_time = 0;

Adafruit_MotorShield AFMS = Adafruit_MotorShield(); 
Adafruit_StepperMotor *stepper_motor = AFMS.getStepper(max_steps, 2); // resolution 200 rpm, port #2 (M3 and M4)
Adafruit_DCMotor *dc_motor = AFMS.getMotor(1);

void setup() {
  Serial.begin(9600);
  AFMS.begin();                               // default frequency 1.6KHz
  stepper_motor->setSpeed(rpm);                     // setting current rpm
  dc_motor->setSpeed(dc_speed);
}

void loop() {
  while(Serial.available() >= 3){
    chosen_motor = Serial.read();
    if(chosen_motor == 0){          // powder dispenser 
      rpm = Serial.read();
      steps = Serial.read();
      stepper_motor->setSpeed(rpm);
      if(rpm != 0){
        stepper_motor->step(steps, BACKWARD, DOUBLE);
        stepper_motor->release();
      }
      else{
        stepper_motor->release();
      }
      Serial.println(rpm);
      Serial.println(steps);
    }
    else if(chosen_motor == 1){     // stirrer
       dc_time = Serial.read();
       dc_speed = Serial.read();
       if(dc_speed != 0)
       {
         dc_motor->setSpeed(dc_speed);
         dc_motor->run(FORWARD);
         for(int i = 0; i < (dc_time/32); i++)
              delay(32 * 1000);
         delay((dc_time%32) * 1000);
         
         dc_motor->run(RELEASE);
       }
       else{
         dc_motor->run(RELEASE);
       }
      Serial.println(dc_time);
      Serial.println(dc_speed);
    }
    
  }
}
