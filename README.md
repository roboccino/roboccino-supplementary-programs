# Roboccino - supplementary programs

This repository contains Arduino programs used in [Roboccino](https://gitlab.com/roboccino/roboccino) project. There are two directories:
- *motorshield_code*: management of a powder dispenser (stepper motor) and a stirrer (DC motor);
- *servo_code*: management of two servos-based devices - a water dispenser and a ramp.
